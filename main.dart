import 'package:flutter/material.dart';
import 'package:design_profile/profile.dart';
import 'package:design_profile/dashboard.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Profile(),
      theme: ThemeData(
          primarySwatch: Colors.indigo,
          accentColor: Color.fromARGB(255, 47, 129, 170),
          scaffoldBackgroundColor: Colors.blueGrey),
      debugShowCheckedModeBanner: false,
    );
  }
}
